perltools

This is a list of some simple sysadmin perl tools I wrote.

Filename | Description
------------ | -------------
check_for_ssl_issues.pl | check ssl certificates for problems
http_log_monitor.pl | check http logs
lazy_admin.pl | replace your lazy sysadmin with a shell script.
restart_services.pl | restart services from badly behaving systems.
